package com.cy.springbootdemo.mapper;

import com.cy.springbootdemo.entity.Advice;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@SpringBootTest
@RunWith(SpringRunner.class)
public class LeaveMapperTest {
    @Autowired
    private LeaveMapper leaveMapper;

    @Test
    public void findAll(){
        System.out.println(leaveMapper.findAll(0,5));
    }
    @Test
    public void findSpringboot(){
        System.out.println(leaveMapper.findSpringbootById("022301931139"));
    }
}
