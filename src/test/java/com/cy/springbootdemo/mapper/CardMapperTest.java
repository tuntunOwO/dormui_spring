package com.cy.springbootdemo.mapper;

import com.cy.springbootdemo.entity.Advice;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CardMapperTest {
    @Autowired
    private CardMapper cardMapper;

    @Test
    public void findAll(){
        System.out.println(cardMapper.findAll(0,5));
    }
    @Test
    public void updateCard(){
        System.out.println(cardMapper.updateCard("1",new Date()));
    }

}
