package com.cy.springbootdemo.mapper;

import com.cy.springbootdemo.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserMapperTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void findAdminByUserId(){
        System.out.println(userMapper.findStudentByUserId("022300190127"));
    }
    @Test
    public void findStudentByUserId(){
        System.out.println(userMapper.findAdminByUserId("022301931139"));
    }
    @Test
    public void findAllAdmin(){
        System.out.println(userMapper.findAllAdmin(10,10));
    }
    @Test
    public void updateStudentInfo(){
        User user = new User();
        user.setUserId("022301931139");
        user.setPassword("1");
        user.setPhone("123456789");
        System.out.println(userMapper.updateStudentInfo(user));
    }
    @Test
    public void findStudentByPhone(){
        System.out.println(userMapper.findStudentByPhone("123456789"));
    }

}
