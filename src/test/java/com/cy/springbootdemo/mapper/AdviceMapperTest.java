package com.cy.springbootdemo.mapper;

import com.cy.springbootdemo.entity.Advice;
import com.cy.springbootdemo.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AdviceMapperTest {
    @Autowired
    private AdviceMapper adviceMapper;

    @Test
    public void findAll(){
        System.out.println(adviceMapper.findAll(0,5));
    }
    @Test
    public void insertAdvice(){
        Advice advice = new Advice();
        advice.setUserId("022301901132");
        advice.setAuthor("测试");
        advice.setTitle("真不好说");
        advice.setContent("不好说");
        advice.setCtime(new Date());
        System.out.println(adviceMapper.insertAdvice(advice));
    }
    @Test
    public void updateAdvice(){
        Advice advice = new Advice();
        advice.setUserId("022301901132");
        advice.setAuthor("测试");
        advice.setTitle("真好说");
        advice.setContent("好说");
        advice.setCtime(new Date());
        System.out.println(adviceMapper.updateAdvice(advice));
    }
}
