package com.cy.springbootdemo.mapper;

import com.cy.springbootdemo.entity.User;

import java.util.List;

public interface UserMapper {

    Integer findAllAdminCount();

    List<User> findAllAdmin(Integer index, Integer rows);

    List<User> findAllStudent(Integer index,Integer rows);

    User findStudentByUserId(String userId);

    User findAdminByUserId(String userId);

    User findStudentByPhone(String phone);

    User findAdminByPhone(String phone);

    Integer updateAdminInfo(User user);

    Integer updateStudentInfo(User user);

    List<User> findAdminByName(String name);

    Integer updateStudentAvatar(User user);

    Integer updateAdminAvatar(User user);
}
