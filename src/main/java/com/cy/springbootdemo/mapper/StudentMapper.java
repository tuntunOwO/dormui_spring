package com.cy.springbootdemo.mapper;

import com.cy.springbootdemo.entity.Advice;
import com.cy.springbootdemo.entity.Dormm;
import com.cy.springbootdemo.entity.Student;
import com.cy.springbootdemo.entity.User;
import com.sun.xml.internal.bind.v2.TODO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StudentMapper {
    //TODO
    //新增学生的时候记得新增学生的打卡数据
    Integer findAllCount();

    List<User> findAll(Integer index, Integer rows);

    List<User> findByName(Integer index, Integer rows,String name);


    String findUserIdById(Integer id);

    Integer insertStudent(User user);

    Integer insertStudentDorm(User user);

    Integer updateStudent(User user);

    Integer updateStudentDorm(String stuId,Integer floor, Integer dormId);

    Integer deleteStudent(Integer id);

    Dormm findDormmByStuId(String stuId);

    Integer deleteByIds(Integer[] id);
}
