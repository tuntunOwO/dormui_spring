package com.cy.springbootdemo.mapper;

import com.cy.springbootdemo.entity.Violation;

import java.util.List;

public interface ViolationMapper {
    Integer findAllCount();

    List<Violation> findAll(Integer index,Integer rows);

    Integer insertViolation(Violation violation);

    Integer updateViolation(Violation violation);

    Integer deleteViolation(Integer id);
}
