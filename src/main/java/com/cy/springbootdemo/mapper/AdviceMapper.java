package com.cy.springbootdemo.mapper;

import com.cy.springbootdemo.entity.Advice;

import java.util.List;

public interface AdviceMapper {
    List<Advice> findAll(Integer index,Integer rows);

     Integer insertAdvice(Advice advice);

    Integer updateAdvice(Advice advice);

    Integer deleteAdvice(Integer id);

    Integer findAllCount();
}
