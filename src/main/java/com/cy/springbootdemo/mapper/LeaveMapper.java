package com.cy.springbootdemo.mapper;

import com.cy.springbootdemo.entity.Leave;

import java.util.Date;
import java.util.List;

public interface LeaveMapper {

    Integer findAllCount();

    List<Leave> findAll(Integer index, Integer rows);

    Integer insertLeave(Leave leave);

    Integer deleteLeave(String stuId);

    Integer updateLeave(String stuId, Integer springboot);

    Integer findSpringbootById(String stuId);
}
