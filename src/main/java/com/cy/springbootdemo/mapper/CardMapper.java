package com.cy.springbootdemo.mapper;

import com.cy.springbootdemo.entity.Card;

import java.util.Date;
import java.util.List;

public interface CardMapper {
    Integer findAllCount();

    List<Card> findAll(Integer index, Integer rows);

    Integer findNoCartCount();

    List<Card> findNoCard(Integer index, Integer rows);

    Integer updateCard(String stuId, Date ctime);

}
