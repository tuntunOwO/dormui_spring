package com.cy.springbootdemo.mapper;

import com.cy.springbootdemo.entity.BadGoods;

import java.util.Date;
import java.util.List;

public interface BadGoodsMapper {

     BadGoods findById(Integer id);

     Integer findAllCount();

     List<BadGoods> findAllBadGoods(Integer index, Integer rows);

     List<BadGoods> findByGoods(String goods);

     Integer deleteBadGoods(Integer id);

     Integer insertBadGoods(BadGoods badGoods);

     Integer updateBadGoods(BadGoods badgoods);
}
