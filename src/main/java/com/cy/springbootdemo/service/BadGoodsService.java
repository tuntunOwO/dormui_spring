package com.cy.springbootdemo.service;

import com.cy.springbootdemo.entity.BadGoods;

import java.util.Date;
import java.util.List;

public interface BadGoodsService {

    Integer findAllCount();

    List<BadGoods> findByGoods(String goods);

    List<BadGoods> findAllBadGoods(Integer page, Integer rows);

    void deleteBadGoods(Integer id);

    void insertBadGoods(BadGoods badGoods);

    void updateBadGoods(Integer id);

}
