package com.cy.springbootdemo.service;

import com.cy.springbootdemo.entity.Advice;
import com.cy.springbootdemo.entity.Card;

import java.util.List;

public interface CardService {
    Integer findAllCount();

    List<Card> findAll(Integer page, Integer rows);

    Integer findNoCartCount();

    List<Card> findNoCard(Integer page, Integer rows);

    void updateCard(String stuId);

}
