package com.cy.springbootdemo.service;

import com.cy.springbootdemo.entity.Violation;

import java.util.List;

public interface ViolationService {
    List<Violation> findAll(Integer page, Integer rows);

    Integer findAllCount();

    void insertViolation(Violation violation);

    void updateViolation(Violation violation);

    void deleteViolation(Integer id);
}
