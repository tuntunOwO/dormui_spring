package com.cy.springbootdemo.service;

import com.cy.springbootdemo.entity.User;

import java.util.List;

public interface StudentService {
    List<User> findAllStudent(Integer page, Integer rows);

    Integer findAllCount();

    List<User> findByName(Integer page, Integer rows,String name);

    void updateStudent(User user);

    void insertStudent(User user);

    void deleteStudent(Integer id);

    void deleteStudentById(Integer[] id);

}
