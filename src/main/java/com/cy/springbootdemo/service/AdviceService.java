package com.cy.springbootdemo.service;

import com.cy.springbootdemo.entity.Advice;

import java.util.List;

public interface AdviceService {
    List<Advice> findAll(Integer page,Integer rows);

    void insertAdvice(Advice advice);

    void updateAdvice(Advice advice);

    void deleteAdvice(Integer id);

    Integer findAllCount();
}
