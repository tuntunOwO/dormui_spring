package com.cy.springbootdemo.service.impl;

import com.cy.springbootdemo.entity.Card;
import com.cy.springbootdemo.mapper.CardMapper;
import com.cy.springbootdemo.service.CardService;
import com.cy.springbootdemo.service.ex.ServiceException;
import com.cy.springbootdemo.service.ex.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CardServiceImpl implements CardService {
    @Autowired
    private CardMapper cardMapper;

    @Override
    public Integer findAllCount() {
        return cardMapper.findAllCount();
    }

    @Override
    public List<Card> findAll(Integer page, Integer rows) {
        Integer index = (page-1)*rows;
        return cardMapper.findAll(index,rows);
    }

    @Override
    public Integer findNoCartCount() {
        return cardMapper.findNoCartCount();
    }

    @Override
    public List<Card> findNoCard(Integer page, Integer rows) {
        Integer index = (page-1)*rows;
        return cardMapper.findNoCard(index,rows);
    }

    @Override
    public void updateCard(String stuId) {
        System.out.println(stuId);
        Integer row = cardMapper.updateCard(stuId,new Date());

        if (row != 1) {
            throw new UpdateException("打卡更新数据时产生未知异常");
        }
    }

}
