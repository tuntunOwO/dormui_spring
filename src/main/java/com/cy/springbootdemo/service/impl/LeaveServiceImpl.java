package com.cy.springbootdemo.service.impl;

import com.cy.springbootdemo.entity.Dormm;
import com.cy.springbootdemo.entity.Leave;
import com.cy.springbootdemo.entity.User;
import com.cy.springbootdemo.mapper.LeaveMapper;
import com.cy.springbootdemo.mapper.StudentMapper;
import com.cy.springbootdemo.service.LeaveService;
import com.cy.springbootdemo.service.ex.DeleteException;
import com.cy.springbootdemo.service.ex.InsertException;
import com.cy.springbootdemo.service.ex.ServiceException;
import com.cy.springbootdemo.service.ex.UpdateException;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public class LeaveServiceImpl implements LeaveService {
    @Autowired
    private LeaveMapper leaveMapper;
    @Autowired
    private StudentMapper studentMapper;

    @Override
    public Integer findAllCount() {
        return leaveMapper.findAllCount();
    }

    @Override
    public List<Leave> findAll(Integer page, Integer rows) {
        Integer index = (page-1)*rows;
        List<Leave> list = leaveMapper.findAll(index,rows);
        /*if (list.size() == 0) {
            throw new ServiceException("不知道，反正就是有异常");
        }*/
        return list;
    }

    @Override
    public void insertLeave(Leave leave) {
        Dormm dormm = studentMapper.findDormmByStuId(leave.getStuId());
        if (dormm == null) {
            throw new ServiceException("发生未知异常");
        }
        leave.setDormId(dormm.getDormId());
        leave.setFloor(dormm.getFloor());
        leave.setLTime(new Date());
        Integer row = leaveMapper.insertLeave(leave);
        if (row != 1) {
            throw new InsertException("新增离校数据产生未知异常");
        }
    }

    @Override
    public void deleteLeave(String stuId) {
        Integer row = leaveMapper.deleteLeave(stuId);
        if (row != 1) {
            throw new DeleteException("删除离校记录失败");
        }
    }

    @Override
    public void updateLeave(String stuId, Integer springboot) {
        Integer row = leaveMapper.updateLeave(stuId, springboot);
        if (row != 1) {
            throw new UpdateException("更新离校记录失败");
        }
    }

    @Override
    public Integer findSpringbootById(String stuId) {
        Integer spring = leaveMapper.findSpringbootById(stuId);
        System.out.println(spring);
        if (spring == null) {
            //表示该学生非常听话，没有提交离校审批
            System.out.println(111);
            return 3;
        }
        return spring;
    }
}
