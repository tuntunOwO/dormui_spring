package com.cy.springbootdemo.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.cy.springbootdemo.Dto.UserAndCode;
import com.cy.springbootdemo.entity.User;
import com.cy.springbootdemo.mapper.UserMapper;
import com.cy.springbootdemo.service.UserService;
import com.cy.springbootdemo.service.ex.*;
import com.cy.springbootdemo.util.PuzzleCaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer findAllAdminCount() {
        return userMapper.findAllAdminCount();
    }

    @Override
    public List<User> findAllAdmin(Integer page, Integer rows) {
        Integer index = (page-1)*rows;
        List<User> list = userMapper.findAllAdmin(index,rows);
        if (list.size() == 0) {
            throw new ListDataNotFoundException("不知道，反正就是有异常");
        }
        return list;
    }

    @Override
    public List<User> findAllStudent(Integer page, Integer rows) {
        Integer index = (page-1)*rows;
        List<User> list = userMapper.findAllStudent(index,rows);
        if (list.size() == 0) {
            throw new ListDataNotFoundException("不知道，反正就是有异常");
        }
        return list;
    }


    @Override
    public User login(User user) {
        String phone = user.getPhone();
        String code = user.getCode();
        Integer role = user.getRole();
        String userId = user.getUserId();
        String password = user.getPassword();
        User result;
        if (phone != null) {
            User result1 = userMapper.findStudentByPhone(phone);
            User result2 = userMapper.findAdminByPhone(phone);
            if (result1 == null && result2 == null) {
                throw new UserIdNotFoundException("用户不存在");
            }
            String redisCode = stringRedisTemplate.opsForValue().get("phone:"+user.getPhone());
            if (redisCode == null) {
                throw new CAPTCHANotSendException("请先发送验证码");
            }
            if (code == null) {
                throw new CAPTCHANIsNull("验证码不能为空");
            }
            if (!code.equals(redisCode)) {
                throw new CAPTCHNotMatchException("请输入正确的验证码");
            }
            if (result1 == null) {
                return result2;
            }else {
                return result1;
            }
        } else {
            if (role == 0) {
                result = userMapper.findStudentByUserId(userId);
            }else {
                result = userMapper.findAdminByUserId(userId);
            }
            if (result == null) {
                throw new UserIdNotFoundException("用户id不存在");
            }
            if (!Objects.equals(result.getPassword(), password)) {
                throw new PasswordNotMatchException("用户名与密码不匹配");
            }
        }
        if (!PuzzleCaptchaUtil.CACHE_CODE.containsKey(user.getNonceStr())) {
            //验证码失效
            throw new CAPTCHANotSendException("验证码失效");
        }
        //获取缓存验证码
        Integer value = PuzzleCaptchaUtil.CACHE_CODE.get(user.getNonceStr());
        //根据移动距离判断验证是否成功
        if (Math.abs(value - Integer.parseInt(user.getValue())) > 3) {
            //验证码有误
            throw new CAPTCHNotMatchException("验证码有误");
        }
        return result;
    }
    //验证码登录
   /* @Override
    public User loginByPhone(UserAndCode userAndCode) {
        String phone = userAndCode.getPhone();
        String code = userAndCode.getCode();
        Integer role = userAndCode.getRole();
        User user;
        if (role == 0) {
            user = userMapper.findStudentByPhone(phone);
        }else {
            user = userMapper.findAdminByPhone(phone);
        }
        if (user == null) {
            throw new UserIdNotFoundException("用户不存在");
        }
        String redisCode = stringRedisTemplate.opsForValue().get("phone:"+user.getPhone());
        if (redisCode == null) {
            throw new CAPTCHANotSendException("请先发送验证码");
        }
        if (code == null) {
            throw new CAPTCHANIsNull("验证码不能为空");
        }
        if (!code.equals(redisCode)) {
            throw new CAPTCHNotMatchException("请输入正确的验证码");
        }
        return user;
    }
*/
    @Override
    public List<User> findAdminByName(String name) {
        return userMapper.findAdminByName(name);
    }

    @Override
    public void updateInfo(User user) {
        if (user.getRole() == 0) {
            User result = userMapper.findStudentByUserId(user.getUserId());
            if (result == null) {
                throw new UserIdNotFoundException("用户数据不存在");
            }
            Integer row = userMapper.updateStudentInfo(user);
            if (row != 1) {
                throw new UpdateException("更新时产生未知的异常");
            }
        }else {
            User result = userMapper.findAdminByUserId(user.getUserId());
            if (result == null) {
                throw new UserIdNotFoundException("用户数据不存在");
            }
            Integer row = userMapper.updateAdminInfo(user);
            if (row != 1) {
                throw new UpdateException("更新时产生未知的异常");
            }
        }
    }


    @Override
    public void sendCode(String phone,Integer role) {
        if (phone == null) {
            throw new PhoneIsNullException("请先输入手机号");
        }
        User result1 = userMapper.findStudentByPhone(phone);
        User result2 = userMapper.findAdminByPhone(phone);
        if (result1 == null && result2 == null) {
            throw new UserIdNotFoundException("用户不存在");
        }
        Long expire = stringRedisTemplate.opsForValue().getOperations().getExpire("phone:" + phone);
        if (expire == null || expire <= 240L) {
            String code = RandomUtil.randomNumbers(6);
            stringRedisTemplate.opsForValue().set("phone:"+phone,code,5, TimeUnit.MINUTES);
            System.out.println("手机用户"+phone+"发送的验证码为: " + code);
        }else {
            throw new NotRepeatSendCAPTCHException("60秒内不能重复发验证码");
        }

    }

    @Override
    public void updateAvatar(User user) {
        if (user.getRole() == 0) {
            User result = userMapper.findStudentByUserId(user.getUserId());
            if (result == null) {
                throw new UserIdNotFoundException("用户数据不存在");
            }
            userMapper.updateStudentAvatar(user);
        }else {
            User result = userMapper.findAdminByUserId(user.getUserId());
            if (result == null) {
                throw new UserIdNotFoundException("用户数据不存在");
            }
            userMapper.updateAdminAvatar(user);
        }
    }

}
