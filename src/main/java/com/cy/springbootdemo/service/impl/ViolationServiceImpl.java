package com.cy.springbootdemo.service.impl;

import com.cy.springbootdemo.entity.Violation;
import com.cy.springbootdemo.mapper.ViolationMapper;
import com.cy.springbootdemo.service.ViolationService;
import com.cy.springbootdemo.service.ex.DeleteException;
import com.cy.springbootdemo.service.ex.InsertException;
import com.cy.springbootdemo.service.ex.ServiceException;
import com.cy.springbootdemo.service.ex.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public class ViolationServiceImpl implements ViolationService {
    @Autowired
    private ViolationMapper violationMapper;
    @Override
    public List<Violation> findAll(Integer page, Integer rows) {
        Integer index = (page-1)*rows;
        return violationMapper.findAll(index,rows);
    }

    @Override
    public Integer findAllCount() {
        return violationMapper.findAllCount();
    }

    @Override
    public void insertViolation(Violation violation) {
        violation.setWtime(new Date());
        Integer row = violationMapper.insertViolation(violation);
        if (row != 1) {
            throw new InsertException("不管，就是有异常");
        }
    }

    @Override
    public void updateViolation(Violation violation) {
        violation.setWtime(new Date());
        Integer row = violationMapper.updateViolation(violation);
        if (row != 1) {
            throw new UpdateException("更新数据时产生未知异常");
        }
    }

    @Override
    public void deleteViolation(Integer id) {
        Integer row = violationMapper.deleteViolation(id);
        if (row != 1) {
            throw new DeleteException("更新数据时产生未知异常");
        }
    }
}
