package com.cy.springbootdemo.service.impl;

import com.cy.springbootdemo.entity.User;
import com.cy.springbootdemo.mapper.StudentMapper;
import com.cy.springbootdemo.service.StudentService;
import com.cy.springbootdemo.service.ex.DeleteException;
import com.cy.springbootdemo.service.ex.InsertException;
import com.cy.springbootdemo.service.ex.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentMapper studentMapper;
    @Override
    public List<User> findAllStudent(Integer page, Integer rows) {
        Integer index = (page-1)*rows;
        return studentMapper.findAll(index,rows);
    }

    @Override
    public Integer findAllCount() {
        return studentMapper.findAllCount();
    }

    @Override
    public List<User> findByName(Integer page, Integer rows, String name) {
        Integer index = (page-1)*rows;
        return studentMapper.findByName(index,rows,name);
    }

    @Override
    //TODO
    //不确定不修改算不算更新
    public void updateStudent(User user) {
        Integer row1 = studentMapper.updateStudent(user);
        String stuId = studentMapper.findUserIdById(user.getId());
        Integer row2 = studentMapper.updateStudentDorm(stuId, user.getFloor(), user.getDormId());
    }

    @Override
    public void insertStudent(User user) {
        Integer row1 = studentMapper.insertStudent(user);
        Integer row2 = studentMapper.insertStudentDorm(user);
        if (row2 != 1 && row1 != 1) {
            throw new InsertException("插入操作时产生未知异常");
        }
    }

    @Override
    public void deleteStudent(Integer id) {
        Integer row = studentMapper.deleteStudent(id);
        if (row != 1) {
            throw new DeleteException("删除学生时产生未知异常");
        }
    }

    @Override
    public void deleteStudentById(Integer[] id) {
        Integer rows = studentMapper.deleteByIds(id);
        if (rows != id.length) {
            throw new DeleteException("批量删除出学生时产生未知异常");
        }
    }
}
