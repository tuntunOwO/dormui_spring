package com.cy.springbootdemo.service.impl;

import com.cy.springbootdemo.entity.BadGoods;
import com.cy.springbootdemo.mapper.BadGoodsMapper;
import com.cy.springbootdemo.service.BadGoodsService;
import com.cy.springbootdemo.service.ex.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public class BadGoodsServiceImpl implements BadGoodsService {

    @Autowired
    private BadGoodsMapper badGoodsMapper;

    @Override
    public Integer findAllCount() {
        return badGoodsMapper.findAllCount();
    }

    @Override
    public List<BadGoods> findByGoods(String goods) {
        return badGoodsMapper.findByGoods(goods);
    }

    @Override
    public List<BadGoods> findAllBadGoods(Integer page, Integer rows) {
        Integer index = (page-1)*rows;
        List<BadGoods> list = badGoodsMapper.findAllBadGoods(index,rows);
        if (list == null) {
            throw new ListDataNotFoundException("查询数据产生未知异常");
        }
        return list;
    }

    @Override
    public void deleteBadGoods(Integer id) {
        BadGoods result = badGoodsMapper.findById(id);
        if (result == null) {
            throw new DataNotFoundException("数据不存在");
        }
        Integer row = badGoodsMapper.deleteBadGoods(id);
        if (row != 1) {
            throw new DeleteException("删除数据时产生未知异常");
        }
    }

    @Override
    public void insertBadGoods(BadGoods badGoods) {
        badGoods.setSubtime(new Date());
        Integer row = badGoodsMapper.insertBadGoods(badGoods);
        if (row != 1) {
            throw new InsertException("新增信息时产生未知异常");
        }
    }

    @Override
    public void updateBadGoods(Integer id) {
        BadGoods badGoods = badGoodsMapper.findById(id);
        badGoods.setRsotime(new Date());
        Integer row = badGoodsMapper.updateBadGoods(badGoods);
        if (row != 1) {
            throw new UpdateException("更新产生未知异常");
        }
    }
}
