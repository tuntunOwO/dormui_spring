package com.cy.springbootdemo.service.impl;

import com.cy.springbootdemo.entity.Advice;
import com.cy.springbootdemo.mapper.AdviceMapper;
import com.cy.springbootdemo.service.AdviceService;
import com.cy.springbootdemo.service.ex.DeleteException;
import com.cy.springbootdemo.service.ex.InsertException;
import com.cy.springbootdemo.service.ex.ServiceException;
import com.cy.springbootdemo.service.ex.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
@Service
public class AdviceServiceImpl implements AdviceService {

    @Autowired
    private AdviceMapper adviceMapper;

    @Override
    public List<Advice> findAll(Integer page,Integer rows) {
        Integer index = (page-1)*rows;
        List<Advice> list = adviceMapper.findAll(index,rows);
        /*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:ss:SS");
        for (Advice a: list) {
            a.setCtime(sdf.format(a.getCtime()));
        }*/
        return list;
    }

    @Override
    public void insertAdvice(Advice advice) {
        advice.setCtime(new Date());
        Integer row = adviceMapper.insertAdvice(advice);
        if (row != 1) {
            throw new InsertException("插入数据时产生未知异常");
        }
    }

    @Override
    public void updateAdvice(Advice advice) {
        advice.setCtime(new Date());
        Integer row = adviceMapper.updateAdvice(advice);
        if (row != 1) {
            throw new UpdateException("更新数据时产生未知异常");
        }
    }

    @Override
    public void deleteAdvice(Integer id) {
        Integer row = adviceMapper.deleteAdvice(id);
        if (row != 1) {
            throw new DeleteException("删除数据时产生未知异常");
        }
    }

    @Override
    public Integer findAllCount() {
        return adviceMapper.findAllCount();
    }
}
