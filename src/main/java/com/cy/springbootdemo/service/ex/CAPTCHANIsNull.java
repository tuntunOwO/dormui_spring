package com.cy.springbootdemo.service.ex;

public class CAPTCHANIsNull extends ServiceException{
    public CAPTCHANIsNull() {
    }

    public CAPTCHANIsNull(String message) {
        super(message);
    }

    public CAPTCHANIsNull(String message, Throwable cause) {
        super(message, cause);
    }

    public CAPTCHANIsNull(Throwable cause) {
        super(cause);
    }

    public CAPTCHANIsNull(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
