package com.cy.springbootdemo.service.ex;

public class CAPTCHNotMatchException extends ServiceException{
    public CAPTCHNotMatchException() {
    }

    public CAPTCHNotMatchException(String message) {
        super(message);
    }

    public CAPTCHNotMatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public CAPTCHNotMatchException(Throwable cause) {
        super(cause);
    }

    public CAPTCHNotMatchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
