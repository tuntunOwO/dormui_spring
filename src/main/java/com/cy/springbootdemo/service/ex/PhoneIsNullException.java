package com.cy.springbootdemo.service.ex;

public class PhoneIsNullException extends ServiceException{
    public PhoneIsNullException() {
    }

    public PhoneIsNullException(String message) {
        super(message);
    }

    public PhoneIsNullException(String message, Throwable cause) {
        super(message, cause);
    }

    public PhoneIsNullException(Throwable cause) {
        super(cause);
    }

    public PhoneIsNullException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
