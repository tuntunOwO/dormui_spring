package com.cy.springbootdemo.service.ex;

public class CaptchaNotFoundException extends ServiceException{
    public CaptchaNotFoundException() {
    }

    public CaptchaNotFoundException(String message) {
        super(message);
    }

    public CaptchaNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public CaptchaNotFoundException(Throwable cause) {
        super(cause);
    }

    public CaptchaNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
