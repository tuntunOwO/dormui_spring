package com.cy.springbootdemo.service.ex;

public class NotRepeatSendCAPTCHException extends ServiceException{
    public NotRepeatSendCAPTCHException() {
    }

    public NotRepeatSendCAPTCHException(String message) {
        super(message);
    }

    public NotRepeatSendCAPTCHException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotRepeatSendCAPTCHException(Throwable cause) {
        super(cause);
    }

    public NotRepeatSendCAPTCHException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
