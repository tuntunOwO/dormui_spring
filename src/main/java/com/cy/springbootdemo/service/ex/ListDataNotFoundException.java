package com.cy.springbootdemo.service.ex;

public class ListDataNotFoundException extends ServiceException{
    public ListDataNotFoundException() {
    }

    public ListDataNotFoundException(String message) {
        super(message);
    }

    public ListDataNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ListDataNotFoundException(Throwable cause) {
        super(cause);
    }

    public ListDataNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
