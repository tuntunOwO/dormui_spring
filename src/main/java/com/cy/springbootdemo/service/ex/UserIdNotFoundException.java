package com.cy.springbootdemo.service.ex;

public class UserIdNotFoundException extends ServiceException{
    public UserIdNotFoundException() {
    }

    public UserIdNotFoundException(String message) {
        super(message);
    }

    public UserIdNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserIdNotFoundException(Throwable cause) {
        super(cause);
    }

    public UserIdNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
