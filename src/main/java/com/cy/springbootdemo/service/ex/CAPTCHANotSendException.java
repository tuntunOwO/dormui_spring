package com.cy.springbootdemo.service.ex;

public class CAPTCHANotSendException extends ServiceException{
    public CAPTCHANotSendException() {
    }

    public CAPTCHANotSendException(String message) {
        super(message);
    }

    public CAPTCHANotSendException(String message, Throwable cause) {
        super(message, cause);
    }

    public CAPTCHANotSendException(Throwable cause) {
        super(cause);
    }

    public CAPTCHANotSendException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
