package com.cy.springbootdemo.service;

import com.cy.springbootdemo.entity.Leave;

import java.util.List;

public interface LeaveService {
    Integer findAllCount();

    List<Leave> findAll(Integer page, Integer rows);

    void insertLeave(Leave leave);

    void deleteLeave(String stuId);

    void updateLeave(String stuId, Integer springboot);

    Integer findSpringbootById(String stuId);
}
