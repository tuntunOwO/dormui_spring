package com.cy.springbootdemo.service;

import com.cy.springbootdemo.Dto.UserAndCode;
import com.cy.springbootdemo.entity.User;

import java.util.List;

public interface UserService {

    Integer findAllAdminCount();

    List<User> findAllAdmin(Integer page, Integer rows);

    List<User> findAllStudent(Integer page, Integer rows);

    User login(User user);

    //User loginByPhone(UserAndCode userAndCode);

    List<User> findAdminByName(String name);

    void updateInfo(User user);

    void updateAvatar(User user);

    void sendCode(String phone,Integer role);


}
