package com.cy.springbootdemo.Dto;

import lombok.Data;

@Data
public class UserAndCode {
    private String userId;
    private String password;
    private String phone;
    private String code;
    private Integer role;
}
