package com.cy.springbootdemo.entity;

import lombok.Data;
@Data
public class Student{
    private Integer id;
    private String userId;
    private String name;
    private String password;
    private Integer role;
    private String teacher;
}
