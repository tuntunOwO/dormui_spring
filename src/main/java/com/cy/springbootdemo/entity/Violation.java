package com.cy.springbootdemo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
@Data
public class Violation {
    private Integer id;
    private String stuId;
    private Date wtime;
    private String wreason;
}
