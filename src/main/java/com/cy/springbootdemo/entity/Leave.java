package com.cy.springbootdemo.entity;

import lombok.Data;

import java.util.Date;
@Data
public class Leave {
    private String stuId;
    private Integer floor;
    private Integer dormId;
    private Date lTime;
    private Integer springboot;
}
