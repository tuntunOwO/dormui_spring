package com.cy.springbootdemo.entity;

import lombok.Data;

import javax.print.DocFlavor;

@Data
public class User {
    private Integer id;
    private String userId;
    private String name;
    private String phone;
    private String password;
    private Integer role;
    private Integer manage;
    private String teacher;
    private Integer floor;
    private Integer dormId;
    private String code;
    private String avatar;
    private String nonceStr;
    private String value;

}
