package com.cy.springbootdemo.entity;

import lombok.Data;

@Data
public class Dormm {
    private String stuId;
    private Integer floor;
    private Integer dormId;
}
