package com.cy.springbootdemo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
@Data
public class Advice {
    private Integer id;
    private String userId;
    private String title;
    private String content;
    private String author;
    private Date ctime;
}
