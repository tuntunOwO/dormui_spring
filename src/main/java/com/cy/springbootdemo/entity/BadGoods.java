package com.cy.springbootdemo.entity;

import lombok.Data;

import java.util.Date;
@Data
public class BadGoods {
    private Integer id;
    private String stuId;
    private Date subtime;
    private Date rsotime;
    private String goods;
}
