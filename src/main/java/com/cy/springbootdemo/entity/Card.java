package com.cy.springbootdemo.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Card {
    private Integer id;
    private String stuId;
    private Integer sure;
    private Date ctime;

}
