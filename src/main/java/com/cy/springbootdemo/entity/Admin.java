package com.cy.springbootdemo.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class Admin{
    private Integer id;
    private String userId;
    private String name;
    private String password;
    private Integer role;
    private Integer manage;
}
