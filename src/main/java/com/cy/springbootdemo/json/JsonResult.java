package com.cy.springbootdemo.json;

import jdk.nashorn.internal.codegen.DumpBytecode;
import lombok.Data;

@Data
public class JsonResult<E> {
    private Integer code;
    private Integer total;
    private String msg;
    private E data;

    public JsonResult(Integer code) {
        this.code = code;
    }

    public JsonResult(Throwable e) {
        this.msg = e.getMessage();
    }

    public JsonResult(Integer code, E data) {
        this.code = code;
        this.data = data;
    }

    public JsonResult(Integer code, String message, E data) {
        this.code = code;
        this.msg = message;
        this.data = data;
    }

    public JsonResult(Integer code, Integer total, E data) {
        this.code = code;
        this.total = total;
        this.data = data;
    }
}
