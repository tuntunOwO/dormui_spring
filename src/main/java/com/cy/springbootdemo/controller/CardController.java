package com.cy.springbootdemo.controller;

import com.cy.springbootdemo.entity.Card;
import com.cy.springbootdemo.json.JsonResult;
import com.cy.springbootdemo.service.CardService;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("card")
public class CardController extends BaseController{
    @Autowired
    private CardService cardService;

    @RequestMapping({"","/"})
    public JsonResult<List<Card>> findAll(Integer page, Integer rows){
        Integer total = cardService.findAllCount();
        List<Card> data = cardService.findAll(page, rows);
        return new JsonResult<>(OK,total,data);
    }
    @RequestMapping("no")
    public JsonResult<List<Card>> findNo(Integer page, Integer rows){
        Integer total = cardService.findNoCartCount();
        List<Card> data = cardService.findNoCard(page, rows);
        return new JsonResult<>(OK,total,data);
    }
    @PutMapping("")
    public JsonResult<Void> updateCard(@RequestBody Card c){
        cardService.updateCard(c.getStuId());
        return new JsonResult<>(OK);
    }
}
