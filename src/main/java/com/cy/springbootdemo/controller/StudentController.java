package com.cy.springbootdemo.controller;

import com.cy.springbootdemo.entity.User;
import com.cy.springbootdemo.json.JsonResult;
import com.cy.springbootdemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("student")
@RestController
public class StudentController extends BaseController{
    @Autowired
    private StudentService studentService;
    @RequestMapping({"","/"})
    public JsonResult<List<User>> findAllStudent(Integer page, Integer rows, String name){
        List<User> data;
        Integer total = studentService.findAllCount();
        if (name.equals("")) {
            data = studentService.findAllStudent(page,rows);
        }else {
            data = studentService.findByName(page, rows, name);
        }
        return new JsonResult<>(OK, total, data);
    }
    @RequestMapping("insert")
    public JsonResult<Void> insertStudent(@RequestBody User user){
        studentService.insertStudent(user);
        return new JsonResult<>(OK);
    }
    @PutMapping()
    public JsonResult<Void> updateStudent(@RequestBody User user){
        studentService.updateStudent(user);
        return new JsonResult<>(OK);
    }
    @DeleteMapping("/{id}")
    public JsonResult<Void> deleteStudent(@PathVariable Integer id){
        studentService.deleteStudent(id);
        return new JsonResult<>(OK);
    }
    @RequestMapping("deletes")
    public JsonResult<Void> deleteById(@RequestBody Integer[] id){
        studentService.deleteStudentById(id);
        return new JsonResult<>(OK);
    }
}
