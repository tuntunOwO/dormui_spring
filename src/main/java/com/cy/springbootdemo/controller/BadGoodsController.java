package com.cy.springbootdemo.controller;

import com.cy.springbootdemo.entity.BadGoods;
import com.cy.springbootdemo.json.JsonResult;
import com.cy.springbootdemo.service.BadGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("badGoods")
public class BadGoodsController extends BaseController{
    @Autowired
    private BadGoodsService badGoodsService;

    @RequestMapping({"","/"})
    public JsonResult<List<BadGoods>> findAllBadGoods(Integer page, Integer rows,String goods){
        List<BadGoods> data;
        if (goods.equals("")) {
             data = badGoodsService.findAllBadGoods(page,rows);
        }else {
             data = badGoodsService.findByGoods(goods);
        }

        Integer total = badGoodsService.findAllCount();
        return new JsonResult<>(OK, total, data);
    }
    @RequestMapping("delete/{id}")
    public JsonResult<Void> deleteBadGoods(@PathVariable("id")Integer id){
        badGoodsService.deleteBadGoods(id);
        return new JsonResult<>(OK);
    }
    @RequestMapping("insert")
    public JsonResult<Void> insertBadGoods(@RequestBody BadGoods badGoods){
        badGoodsService.insertBadGoods(badGoods);
        return new JsonResult<Void>(OK);
    }

    @RequestMapping("update")
    public JsonResult<Void> updateBadGoods(@RequestBody BadGoods badGoods){
        badGoodsService.updateBadGoods(badGoods.getId());
        return new JsonResult<>(OK);
    }

}
