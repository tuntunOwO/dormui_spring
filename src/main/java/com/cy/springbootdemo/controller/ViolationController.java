package com.cy.springbootdemo.controller;

import com.cy.springbootdemo.entity.Violation;
import com.cy.springbootdemo.json.JsonResult;
import com.cy.springbootdemo.service.ViolationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("violation")
@RestController
public class ViolationController extends BaseController{
    @Autowired
    private ViolationService violationService;

    @RequestMapping({"","/"})
    public JsonResult<List<Violation>> findAll(Integer page, Integer rows){
        List<Violation> data = violationService.findAll(page,rows);
        Integer total = violationService.findAllCount();
        return new JsonResult<>(OK,total,data);
    }
    @RequestMapping("insert")
    public JsonResult<Void> insertViolation(@RequestBody Violation violation){
        violationService.insertViolation(violation);
        return new JsonResult<>(OK);
    }
    @RequestMapping("update")
    public JsonResult<Void> updateViolation(@RequestBody Violation violation){
        violationService.updateViolation(violation);
        return new JsonResult<>(OK);
    }
    @RequestMapping("delete/{id}")
    public JsonResult<Void> deleteViolation(@PathVariable("id") Integer id){
        violationService.deleteViolation(id);
        return new JsonResult<>(OK);
    }
}
