package com.cy.springbootdemo.controller;



import com.cy.springbootdemo.entity.Captcha;
import com.cy.springbootdemo.entity.User;
import com.cy.springbootdemo.json.JsonResult;
import com.cy.springbootdemo.service.UserService;
import com.cy.springbootdemo.util.PuzzleCaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController extends BaseController{
    @Autowired
    private UserService userService;

    @GetMapping("sendCode")
    public JsonResult<Void> sendCode(String phone,Integer role){
        System.out.println(role);
        userService.sendCode(phone,role);
        return new JsonResult<>(200);
    }

    @RequestMapping("admin")
    public JsonResult<List<User>> findAllAdmin(Integer page,Integer rows,String name){
        List<User> data;
        if (name.equals("")) {
            data = userService.findAllAdmin(page,rows);
        }else {
            data = userService.findAdminByName(name);
        }

        Integer total = userService.findAllAdminCount();
        return new JsonResult<>(OK, total, data);
    }
    @RequestMapping("student")
    public JsonResult<List<User>> findAllStudent(Integer page,Integer rows){
        List<User> data = userService.findAllStudent(page,rows);
        return new JsonResult<>(OK,data);
    }

    @RequestMapping("login")
    public JsonResult<User> login(@RequestBody User user){
        User data = userService.login(user);
        return new JsonResult<>(OK,data);
    }

    @RequestMapping({"avatar"})
    public JsonResult<Void> updateAvatar(@RequestBody User user){
        userService.updateAvatar(user);
        return new JsonResult<>(OK);
    }

    @PutMapping({"","/"})
    public JsonResult<Void> update(@RequestBody User user){
        userService.updateInfo(user);
        return new JsonResult<>(OK);
    }

    @PostMapping("getCaptcha")
    public JsonResult<Captcha> getCaptcha(@RequestBody Captcha captcha) {
        Captcha data = PuzzleCaptchaUtil.getCaptcha(captcha);
        return new JsonResult<>(OK,data);
    }

}
