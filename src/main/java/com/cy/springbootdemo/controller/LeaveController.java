package com.cy.springbootdemo.controller;

import com.cy.springbootdemo.entity.Leave;
import com.cy.springbootdemo.json.JsonResult;
import com.cy.springbootdemo.service.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("leave")
public class LeaveController extends BaseController {
    @Autowired
    private LeaveService leaveService;

    @GetMapping()
    public JsonResult<List<Leave>> findAll(Integer page, Integer rows) {
        Integer total = leaveService.findAllCount();
        List<Leave> data = leaveService.findAll(page, rows);
        return new JsonResult<>(OK, total, data);
    }

    @GetMapping("/spring")
    public JsonResult<Integer> findSpringbootById(String userId) {
        Integer spring = leaveService.findSpringbootById(userId);
        return new JsonResult<>(OK, spring);
    }

    @PostMapping()
    public JsonResult<Void> insertLeave(@RequestBody Leave leave) {
        leaveService.insertLeave(leave);
        return new JsonResult<>(OK);
    }

    @DeleteMapping("/{stuId}")
    public JsonResult<Void> deleteLeave(@PathVariable("stuId") String stuId) {
        leaveService.deleteLeave(stuId);
        return new JsonResult<>(OK);
    }

    @PutMapping()
    public JsonResult<Void> updateLeave(@RequestBody Leave leave) {
        leaveService.updateLeave(leave.getStuId(), leave.getSpringboot());
        return new JsonResult<>(OK);
    }
}
