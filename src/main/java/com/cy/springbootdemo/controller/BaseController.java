package com.cy.springbootdemo.controller;

import com.cy.springbootdemo.json.JsonResult;
import com.cy.springbootdemo.service.ex.*;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpSession;
@ControllerAdvice
public class BaseController {
    protected final int OK = 200;
    @ExceptionHandler({ServiceException.class})
    public JsonResult<Void> handleException(Throwable e) {
        JsonResult<Void> result = new JsonResult<>(e);
        if (e instanceof UserIdNotFoundException) {
            result.setMsg("用户不存在");
            result.setCode(4000);
        } else if (e instanceof PasswordNotMatchException) {
            result.setMsg("用户名与密码不匹配");
            result.setCode(4001);
        } else if (e instanceof PhoneIsNullException) {
            result.setMsg("手机号不存在");
            result.setCode(4002);
        } else if (e instanceof DataNotFoundException) {
            result.setMsg("操作的数据不存在");
            result.setCode(5000);
        } else if (e instanceof ListDataNotFoundException) {
            result.setMsg("查询数据结果为空");
            result.setCode(5001);
        } else if (e instanceof InsertException) {
            result.setMsg("插入数据时产生未知异常");
            result.setCode(4001);
        } else if (e instanceof DeleteException) {
            result.setMsg("删除数据时产生未知异常");
            result.setCode(4002);
        } else if (e instanceof UpdateException) {
            result.setMsg("编辑数据时产生未知异常");
            result.setCode(4003);
        } else if (e instanceof CAPTCHANIsNull) {
            result.setMsg("验证码不能为空");
            result.setCode(6000);
        } else if (e instanceof CAPTCHANotSendException) {
            result.setMsg("请先发送验证码");
            result.setCode(6001);
        } else if (e instanceof CAPTCHNotMatchException) {
            result.setMsg("验证码错误");
            result.setCode(6002);
        } else if (e instanceof NotRepeatSendCAPTCHException) {
            result.setMsg("60秒内只能发一次验证码");
            result.setCode(6003);
        } else if (e instanceof CaptchaNotFoundException) {
            result.setMsg("验证码失效");
            result.setCode(6003);
        }
        return result;
    }
    final Integer getUidFromSession(HttpSession session) {
        return Integer.valueOf(session.getAttribute("uid").toString());
    }
    //获取当前登录的用户名
    final String getUsernameFromSession(HttpSession session){
        return session.getAttribute("username").toString();
    }
}
