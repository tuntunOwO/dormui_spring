package com.cy.springbootdemo.controller;

import com.cy.springbootdemo.entity.Advice;
import com.cy.springbootdemo.json.JsonResult;
import com.cy.springbootdemo.service.AdviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("advice")
public class AdviceController extends BaseController{
    @Autowired
    private AdviceService adviceService;

    @RequestMapping({"","/"})
    public JsonResult<List<Advice>> findAdvice(Integer page,Integer rows){
        List<Advice> data = adviceService.findAll(page,rows);
        Integer total = adviceService.findAllCount();
        return new JsonResult<>(OK,total,data);
    }
    @RequestMapping("/add")
    public JsonResult<Void> insertAdvice(@RequestBody Advice advice){
        adviceService.insertAdvice(advice);
        return new JsonResult<>(OK);
    }

    @RequestMapping("/update")
    public JsonResult<Void> updateAdvice(@RequestBody Advice advice){
        adviceService.updateAdvice(advice);
        return new JsonResult<>(OK);
    }

    @RequestMapping("/delete/{id}")
    public JsonResult<Void> deleteAdvice(@PathVariable("id")Integer id){
        adviceService.deleteAdvice(id);
        return new JsonResult<>(OK);
    }
}
